﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _03.MarkomApplication.ModelView;
using _04.MarkomApplication.ModelData;


namespace _02.MarkomApplication.ModelAccess
{
    public class TSouvenirModelAccess
    {
        public static string Message;
        public static string DataKosong;
        public static List<TSouvenirModelView> GetListAll(string searchCode, string SearchReceived, DateTime? searchReceivedDate, DateTime? searchCreatedDate, string searchCreated)
        {
            List<TSouvenirModelView> listData = new List<TSouvenirModelView>();
            using (var db = new Db_MarkomEntities())
            {
                listData = (from a in db.t_souvenir
                            join b in db.m_employee
                            on a.request_by equals b.id
                            where a.is_delete == false
                            select new TSouvenirModelView
                            {
                                id = a.id,
                                code = a.code,
                                receivedBy = a.received_by,
                                received = b.first_name + " " + b.last_name,
                                receivedDate = a.received_date,
                                isDelete = a.is_delete,
                                createdBy = a.created_by,
                                created = b.created_by,
                                createdDate = a.created_date,
                            }).ToList();
            }
            if (!string.IsNullOrEmpty(searchCode))
            {
                listData = listData.Where(x => x.code == searchCode).ToList();
            }
            if (!string.IsNullOrEmpty(SearchReceived))
            {
                listData = listData.Where(x => x.received == SearchReceived).ToList();
            }
            if (searchReceivedDate != null)
            {
                listData = listData.Where(x => x.receivedDate.Value.ToString("dd MMMM yyyy") == searchReceivedDate.Value.ToString("dd MMMM yyyy")).ToList();
            }
            if (searchCreatedDate != null)
            {
                listData = listData.Where(x => x.createdDate.Value.ToString("dd MMMM yyyy") == searchCreatedDate.Value.ToString("dd MMMM yyyy")).ToList();
            }
            if (!string.IsNullOrEmpty(searchCreated))
            {
                listData = listData.Where(x => x.created == searchCreated).ToList();
            }
            if (listData.Count == 0)
            {
                DataKosong = "Data tidak ditemukan";
            }
            else
            {
                DataKosong = "";
            }

            return listData;
        }

        public static TSouvenirModelView GetDetailById(int Id)
        {
            TSouvenirModelView result = new TSouvenirModelView();
            using (var db = new Db_MarkomEntities())
            {
                result = (from a in db.t_souvenir
                          where a.id == Id
                          select new TSouvenirModelView
                          {
                              id = a.id,
                              code = a.code,
                              note = a.note,
                              receivedBy = a.received_by,
                              receivedDate = a.received_date,
                          }).FirstOrDefault();
            }
            return result;
        }

        public static List<EmployeeModelView> receivedName()
        {
            List<EmployeeModelView> listNote = new List<EmployeeModelView>();
            using (var db = new Db_MarkomEntities())
            {
                listNote = (from a in db.m_employee
                            where a.is_delete == false
                            select new EmployeeModelView
                            {
                                id = a.id,
                                fullName = a.first_name + " " + a.last_name
                            }).ToList();
            }
            return listNote;
        }

        public static List<SouvenirModelView> souvenirName()
        {
            List<SouvenirModelView> listNote = new List<SouvenirModelView>();
            using (var db = new Db_MarkomEntities())
            {
                listNote = (from a in db.m_souvenir
                            where a.is_delete == false
                            select new SouvenirModelView
                            {
                                id = a.id,
                                name = a.name
                            }).ToList();
            }
            return listNote;
        }

        public static List<TSouvenirItemModelView> listItem(int Id)
        {
            List<TSouvenirItemModelView> listNote = new List<TSouvenirItemModelView>();
            using (var db = new Db_MarkomEntities())
            {
                listNote = (from a in db.t_souvenir_item
                            join b in db.m_souvenir
                            on a.m_souvenir_id equals b.id
                            where a.is_delete == false && a.t_souvenir_id == Id
                            select new TSouvenirItemModelView
                            {
                                mSouvenirId = a.m_souvenir_id,
                                list = b.name,
                                qty = a.qty,
                                note = a.note
                            }).ToList();
            }
            return listNote;
        }


        public static bool Insert(TSouvenirModelView paramModel, List<Array> mItem, int counter)
        {
            bool result = true;
            try
            {
                using (var db = new Db_MarkomEntities())
                {
                    t_souvenir a = new t_souvenir();
                    a.type = "additional";
                    a.code = paramModel.code;
                    a.received_by = paramModel.receivedBy;
                    a.received_date = paramModel.receivedDate;
                    a.request_by = paramModel.receivedBy.Value;
                    a.note = paramModel.note;
                    a.is_delete = paramModel.isDelete;
                    a.created_by = paramModel.createdBy;
                    a.created_date = paramModel.createdDate;

                    db.t_souvenir.Add(a);
                    db.SaveChanges();
                }

                using (var db = new Db_MarkomEntities())
                {
                    t_souvenir_item b = new t_souvenir_item();
                    t_souvenir cekid = db.t_souvenir.OrderByDescending(x => x.id).First();
                    for (var i = 0; i < counter; i++)
                    {
                        Array index1 = mItem[i];
                        String get_mId = String.Format("{0}", index1.GetValue(0));
                        String get_qty = String.Format("{0}", index1.GetValue(1));

                        b.t_souvenir_id = cekid.id;
                        b.m_souvenir_id = int.Parse(get_mId);
                        b.qty = int.Parse(get_qty);
                        b.note = String.Format("{0}", index1.GetValue(2));
                        b.is_delete = false;

                        db.t_souvenir_item.Add(b);
                        db.SaveChanges();
                    };
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }


        public static bool Update(TSouvenirModelView paramModel, List<Array> mItem, int counter)
        {
            bool result = true;
            try
            {
                using (var db = new Db_MarkomEntities())
                {
                    t_souvenir a = db.t_souvenir.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (a != null)
                    {
                        a.received_by = paramModel.receivedBy;
                        a.received_date = paramModel.receivedDate;
                        a.request_by = paramModel.receivedBy.Value;
                        a.note = paramModel.note;
                        a.updated_by = paramModel.updatedBy;
                        a.updated_date = paramModel.updatedDate;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }

                //using (var db = new Db_MarkomEntities())
                //{
                //    t_souvenir_item b = new t_souvenir_item();
                //    t_souvenir cekid = db.t_souvenir.OrderByDescending(x => x.id).First();
                //    for (var i = 0; i < counter; i++)
                //    {
                //        Array index1 = mItem[i];
                //        String get_mId = String.Format("{0}", index1.GetValue(0));
                //        String get_qty = String.Format("{0}", index1.GetValue(1));

                //        b.t_souvenir_id = cekid.id;
                //        b.m_souvenir_id = int.Parse(get_mId);
                //        b.qty = int.Parse(get_qty);
                //        b.note = String.Format("{0}", index1.GetValue(2));
                //        b.is_delete = false;

                //        db.t_souvenir_item.Add(b);
                //        db.SaveChanges();
                //    };
                //}
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }















    }
}
